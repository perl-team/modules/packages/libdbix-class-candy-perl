libdbix-class-candy-perl (0.005004-1) unstable; urgency=medium

  * Import upstream version 0.005004.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Nov 2024 01:49:08 +0100

libdbix-class-candy-perl (0.005003-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:24:14 +0100

libdbix-class-candy-perl (0.005003-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * New upstream version 0.005003
  * drop libstring-camelcase-perl from (build) dependencies
  * declare conformance with Policy 4.1.1
  * update years of upstream copyright

 -- Damyan Ivanov <dmn@debian.org>  Sun, 29 Oct 2017 16:22:31 +0000

libdbix-class-candy-perl (0.005002-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Import upstream version 0.005002.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop versioned build dependency on Test::More.
    Already satisfied in oldstable.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Jul 2016 17:13:23 +0200

libdbix-class-candy-perl (0.005001-1) unstable; urgency=medium

  * Import upstream version 0.005001.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Nov 2015 16:23:51 +0100

libdbix-class-candy-perl (0.005000-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.005000
  * Bump debhelper compatibility level to 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 15 Sep 2015 21:47:02 -0300

libdbix-class-candy-perl (0.004001-1) unstable; urgency=medium

  * Import upstream version 0.004001.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 18:50:44 +0200

libdbix-class-candy-perl (0.004000-1) unstable; urgency=medium

  * Import upstream version 0.004000.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jun 2015 15:39:32 +0200

libdbix-class-candy-perl (0.002106-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Rafael Kitover from Uploaders. Not active anymore since several
    years. Thanks for your past work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 0.002106
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Thu, 23 Oct 2014 21:48:49 +0200

libdbix-class-candy-perl (0.002105-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 05 May 2014 22:00:05 +0200

libdbix-class-candy-perl (0.002104-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 23:21:44 +0200

libdbix-class-candy-perl (0.002100-1) unstable; urgency=low

  * Swap order of alternative (build) dependencies after the perl 5.14
    transition.
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Mar 2012 18:49:14 +0100

libdbix-class-candy-perl (0.002001-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * New upstream release.
  * Add build-dep on libtest-fatal-perl.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 11 Aug 2011 14:36:26 +0200

libdbix-class-candy-perl (0.002000-2) unstable; urgency=low

  * Bump build dependency on Test::More (to get the automatic done_testing()
    for subtests). Closes: #627231
  * Set Standards-Version to 3.9.2 (no changes).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 May 2011 00:10:07 +0200

libdbix-class-candy-perl (0.002000-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 10 Mar 2011 20:05:31 -0500

libdbix-class-candy-perl (0.001006-1) unstable; urgency=low

  * New upstream release
  * Rewrite control description
  * Add myself to Uploaders and Copyright
  * Refresh copyright information
  * Bump to debhelper compat 8

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 02 Mar 2011 22:40:32 -0500

libdbix-class-candy-perl (0.001005-1) unstable; urgency=low

  * Initial release. (Closes: #608030: ITP: libdbix-class-candy-perl --
    module that provides syntactic sugar for result classes for the
    DBIx::Class ORM)

 -- Rafael Kitover <rkitover@cpan.org>  Sun, 26 Dec 2010 09:13:22 -0500
